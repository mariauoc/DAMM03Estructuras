/*
 * Entidad Actividad
 */
package damm03estructuras;

import java.util.Objects;

/**
 *
 * @author profesores.linkia
 */
public class Actividad {
    
    private String nombre;
    private double precio;
    private int numPlazas;

    public Actividad(String nombre, double precio, int numPlazas) {
        this.nombre = nombre;
        this.precio = precio;
        this.numPlazas = numPlazas;
    }
    
    

    public int getNumPlazas() {
        return numPlazas;
    }

    public void setNumPlazas(int numPlazas) {
        this.numPlazas = numPlazas;
    }


    public double getPrecio() {
        return precio;
    }

    public void setPrecio(double precio) {
        this.precio = precio;
    }


    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 89 * hash + Objects.hashCode(this.nombre);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Actividad other = (Actividad) obj;
        return other.nombre.equalsIgnoreCase(nombre);
    }

    @Override
    public String toString() {
        return "Actividad{" + "nombre=" + nombre + ", precio=" + precio + ", numPlazas=" + numPlazas + '}';
    }

    
    
}
