/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package damm03estructuras;

import java.util.Objects;

/**
 *
 * @author profesores.linkia
 */
public class ClavePersona {
    private String nombre;
    private String apellido;

    public ClavePersona(String nombre, String apellido) {
        this.nombre = nombre;
        this.apellido = apellido;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 11 * hash + Objects.hashCode(this.nombre);
        hash = 11 * hash + Objects.hashCode(this.apellido);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final ClavePersona other = (ClavePersona) obj;
//        if (!Objects.equals(this.nombre, other.nombre)) {
//            return false;
//        }
//        if (!Objects.equals(this.apellido, other.apellido)) {
//            return false;
//        }
//    
        return other.nombre.equals(nombre) && other.apellido.equals(apellido);
    }
    
    

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellido() {
        return apellido;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }
    
    
}
