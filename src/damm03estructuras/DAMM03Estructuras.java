/*
 * Estructuras de datos avanzadas
 */
package damm03estructuras;

import java.util.ArrayList;
import java.util.HashMap;

/**
 *
 * @author profesores.linkia
 */
public class DAMM03Estructuras {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // Ejemplo de ArrayList con tipos de datos mezclados
        ArrayList ejemplo = new ArrayList();
        ejemplo.add("Hola");
        ejemplo.add(2);
        ejemplo.add('M');
        ejemplo.add(34.5);

        ArrayList<Integer> numeros = new ArrayList<>();
        System.out.println(ejemplo.get(3));
        ejemplo.add(1, "Aqui");
        
         for (Object o : ejemplo) {
            System.out.println(o);
            if (o instanceof Integer) {
                int numero = ((Integer) o).intValue()+ 100;
                System.out.println(numero);
            }
        }
         
        ArrayList<Actividad> actividades = new ArrayList<>();
        Actividad a1 = new Actividad("Zumba", 20, 10);
        Actividad a2 = new Actividad("Karate", 30, 10);
        Actividad a3 = new Actividad("Taekwondo", 20, 10);

        actividades.add(a1);
        actividades.add(a2);
        actividades.add(a3);

//         Actividad a4 = a1;
        Actividad a4 = new Actividad("Zumba", 50, 15);
        if (actividades.contains(a4)) {
            System.out.println("Si la contiene");
        } else {
            System.out.println("No la contiene");
        }
        if (a2.equals(a3)) {
            System.out.println("Son iguales con equals");
        } else {
            System.out.println("No son iguales.");
        }
        HashMap<String, Actividad> misActividades = new HashMap<>();
        misActividades.put(a1.getNombre(), a1);
        misActividades.put(a2.getNombre(), a2);
        misActividades.put(a3.getNombre(), a3);
        misActividades.put(a4.getNombre(), a4);
        if (misActividades.containsKey("Zumba")) {
            System.out.println("Hay una actividad que es zumba");
        } else {
            System.out.println("No la hay");
        }
        if (misActividades.containsValue(a4)) {
            System.out.println("Si ");
        } else {
            System.out.println("no");
        }
        
        for (Actividad a : misActividades.values()) {
            System.out.println(a);
        }

        // Ejemplo complejo
        HashMap<ClavePersona, Persona> misPersonas = new HashMap<>();
    }

}
