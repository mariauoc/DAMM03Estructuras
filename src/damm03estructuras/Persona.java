/*
 * Ejemplo un poco mas complejo
 */
package damm03estructuras;

import java.util.Objects;

/**
 *
 * @author profesores.linkia
 */
public class Persona {
    
    private ClavePersona nombreCompleto;
    private int edad;

    public Persona(ClavePersona nombreCompleto, int edad) {
        this.nombreCompleto = nombreCompleto;
        this.edad = edad;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 71 * hash + Objects.hashCode(this.nombreCompleto);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Persona other = (Persona) obj;
        if (!Objects.equals(this.nombreCompleto, other.nombreCompleto)) {
            return false;
        }
        return true;
    }
    
    

    public ClavePersona getNombreCompleto() {
        return nombreCompleto;
    }

    public void setNombreCompleto(ClavePersona nombreCompleto) {
        this.nombreCompleto = nombreCompleto;
    }

  
    

    public int getEdad() {
        return edad;
    }

    public void setEdad(int edad) {
        this.edad = edad;
    }


}
